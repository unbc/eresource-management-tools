# eResource Management Tools

## Overview

**Purpose:** This collection of Python scripts arose out of a need/desire to automate some tedious tasks surrounding the management of eResource records in the Sierra ILS (Integrated Library System). Some of them are not Sierra-specific, and could prove useful to users of any ILS or LMS.

* **get_kbart_website_status_list:** Parses one or more KBART .xlsx files. Checks the status of each URL provided, then generates a single .csv that lists the URL, Status, and source .csv filename
* **get_kbart_website_status_list_threaded:** same as previous, except it uses threading, so it's _much_ faster. Some providers (eg Wiley) don't appreciate this, hence keeping the single-threaded version around.
* **get-sersol_website_status_list:** Parses one or more .xlsx files containing a list of URLs. Checks the status of each URL provided, then generates a new .csv file that includes all the information from the source file, plus the URL Status. Functionality is essentially identical get_kbart_website_status_list, the only difference is the input file format. This script is set up to parse .xlsx files in the format of Column A: bib#, Column B: 001, Column C: URL. However, the only thing that _really_ matters is that Column C contains the URLs.
* **erm_conversion_uploader:** This one is Sierra-specific. Sierra will not accept properly-formatted ERM Counter Report files. Someone at University of Nebraska - Lincoln Libraries has kindly developed a conversion tool site (http://statsconverter4erm.unl.edu) but it only accepts one file at a time. This script will batch upload all your .csv/.tsv files for conversion, and save the resulting output files. Input files must be in one of the COUNTER JR1 formats accepted by the conversion website.

---

## Usage/Requirements

These Python scripts were designed to be as easy to use as possible. They were developed using Thonny (https://thonny.org), and that is also the easiest way to run them.

If you're comfortable with Python on the command line, installing packages with pip, etc, great! Download the .py files, install the appropriate packages, and run the script(s).

If you're _not_ comfortable with Python/pip/command line, and you use Windows, I highly recommend Thonny. It's a GUI tool that includes a Python install, and an easy way to manage Python packages, without ever having to touch the command prompt.

### Python Version

These scripts were written with **Python 3**, and have _not_ been tested with Python 2.x

### Package Dependencies

Each script has a slightly different set of dependencies. To cover the bases for all scripts, you'll need:

* requests
* bs4
* openpyxl

### Running a script

None of the scripts require arguments to be passed. Just put them in the same folder as the required source .csv/.tsv/.xlsx files (depending on the script), and run the script. Output file(s) will be created in the same directory.

---

## Rights

* Distributed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License (http://creativecommons.org/licenses/by-nc-sa/3.0/). 
* Developed by David Pettitt, Systems Administrator I, UNBC Library

---

## Feedback/Questions

Contact: <neufel2@gmail.com>

Please let us know if you find these tools helpful and/or if you find any issues with these tools.