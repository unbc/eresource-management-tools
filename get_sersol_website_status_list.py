"""
get_sersol_website_status_list script

Written by: David Pettitt (Systems Administrator I, Geoffrey R Weller Library, University of Northern British Columbia)

Written for: Mandi Schwarz

Purpose: Parse a list of SerialsSolutions .xlsx files, test the URLs, generate a new .csv file with the original three columns, plus an extra status column

Distributed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License (http://creativecommons.org/licenses/by-nc-sa/3.0/). 

Questions? Contact Mandi at neufel2@gmail.com

"""

# must install 'requests' package, for HTTP requests
import requests
# must install 'openpyxl' package, for reading Excel files
from openpyxl import Workbook
from openpyxl import load_workbook
# must install BeautifulSoup4
from bs4 import BeautifulSoup
from pathlib import Path
import csv
from time import sleep

# Suppress all the 'InsecureRequestWarning' from using verify=False in the requests
requests.urllib3.disable_warnings()

# Get a list of .xlsx files in current directory
p = Path('.')
filelist = list(p.glob('*.xlsx'))

# This list will store all the results, to be written to .csv
results=['RECORD #(BIBLIO)','001','856 40 |u','Result']

# Output file
out_file = "get_sersol_website_status_list.csv"

# create the destination .csv
with open (out_file, 'w', newline='', encoding='utf-8') as f:
    #wr = csv.writer(f, quoting=csv.QUOTE_ALL)
    wr = csv.writer(f)
    wr.writerow(results)

for file in filelist:
    in_file = str(file)

    # Load workbook from file into 'wb' variable
    wb = load_workbook(in_file)

    # load primary worksheet from workbook into 'ws' variable
    ws = wb._sheets[0]
    
    # open the .csv file for appending
    with open (out_file, 'a', newline='', encoding='utf-8') as f:
        wr = csv.writer(f, quoting=csv.QUOTE_ALL)
        
        # open a requests Session, for all the requests
        with requests.Session() as s:
            #for row in ws.iter_rows(min_row=2,max_col=10,max_row=2):
            for row in ws.iter_rows(min_row=2,max_col=3,max_row=ws.max_row):
                record_num = row[0].value
                ssib = row[1].value
                title_url = row[2].value
                
                # make sure it's not a blank row
                if title_url is not None:
                    print ("Checking ",record_num,"...")
                    
                    # Pre-emptively set this variable. Is hopefully overwritten by one of the lines below
                    status_desc = "Unknown Error"
                    
                    # Request the title_url, check for various errors
                    sleep(0.05) # without this sleep, we get random 503 errors from the server
                    try:
                        r = s.get(title_url, verify=False)
                    except requests.ConnectionError:
                        status_desc = "Connection Error"
                    except requests.TooManyRedirects:
                        status_desc = "Too Many Redirects"
                    except requests.Timeout:
                        status_desc = "Timeout"
                    else:
                        page = r.text
                        #print (r.status_code)
                        #print ("Page variable length is ",len(page))
                    
                    # Parse results with BeautifulSoup
                    soup = BeautifulSoup(page, "html.parser")
                    journal_result = soup.find("div",class_="SS_NoJournalFoundMsg")
                    #print (journal_result)
                    if journal_result is not None:
                        status_desc = journal_result.text
                    journal_result = soup.find("span",class_="SS_JournalTitle")
                    #print (journal_result)
                    if journal_result is not None:
                        status_desc = journal_result.strong.text
                    
                # Blank Row
                else:
                    status_desc = "No URL in source file"
                
                # Overwrite results list with current results
                results=[record_num,ssib,title_url,status_desc]
                
                # Append results to the .csv file
                wr.writerow(results)