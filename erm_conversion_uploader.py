"""
Batch Uploader for ERM Counter Report Conversion tool
http://statsconverter4erm.unl.edu/
Uploads multiple .csv files, and downloads the converted result

Written by David Pettitt
March 11, 2018
"""
# must install 'requests' package
import requests
# must install BeautifulSoup4
from bs4 import BeautifulSoup
from pathlib import Path

# Set values to be used for uploading
url = "http://statsconverter4erm.unl.edu/"
#payload = {'inputformat': 'csv'}
#files = {'filename': open('R3-JR1-csvexample.csv', 'rb')}

# Get a list of .csv and .tsv files in current directory
p = Path('.')
filelist = list(p.glob('*.csv'))
filelist += list(p.glob('*.tsv'))

with requests.Session() as s:
    for file in filelist:
        # Open csv/tsv file for reading
        with open(str(file), 'rb') as upload:
            # Add csv/tsv file to dictionary, to pass with POST request
            files = {'filename': upload}
            
            if str(file)[-3::] == 'csv':
                payload = {'inputformat': 'csv'}
            else:
                payload = {'inputformat': 'tsv'}
                
            
            # POST the file to the form on the webpage
            r = s.post(url,files=files,data=payload)
        
        # Parse the resulting webpage with BeautifulSoup
        page = r.text
        soup = BeautifulSoup(page, "html.parser")
        
        # Find the link for the converted file
        results = soup.find('div',class_="results")
        error = soup.find('div',class_="error")
        if results is not None:
            result_file = results.a.string
            result_file_url = url + results.a['href']
            
            # Download the converted file
            with open(result_file, 'wb') as download:
                x = s.get(result_file_url)
                
                for chunk in x.iter_content(chunk_size=128):
                    download.write(chunk)
        else:
            print (str(file))
            print ("Error: ",error.string)
