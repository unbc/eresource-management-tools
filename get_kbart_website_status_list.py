"""
get_kbart_website_status_list script

Written by: David Pettitt (Systems Administrator I, Geoffrey R Weller Library, University of Northern British Columbia)

Written for: Mandi Schwarz

Purpose: Parse a list of KBART .csv files, test the URLs, generate a new .csv file that lists the URL, Status, and name of file the URL came from

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

Questions? Contact Mandi at neufel2@gmail.com

"""

# must install 'requests' package, for HTTP requests
import requests
# 'time' package used to add delay for scanning Wiley
import time        
# must install 'openpyxl' package, for reading Excel files
from openpyxl import Workbook
from openpyxl import load_workbook
from pathlib import Path
import csv

# Suppress all the 'InsecureRequestWarning' from using verify=False in the requests
requests.urllib3.disable_warnings()

# Get a list of .xlsx files in sub-directory
p = Path('.')
filelist = list(p.glob('*.xlsx'))

# This list will store all the results, to be written to .csv
results=[]
results.append(['Publication Title','Status Code','Status Description','URL','File'])

# Output file
out_file = "get_kbart_website_status_list.csv"

for file in filelist:
    in_file = str(file)

    # Load workbook from file into 'wb' variable
    wb = load_workbook(in_file)

    # load primary worksheet from workbook into 'ws' variable
    ws = wb._sheets[0]

    with requests.Session() as s:
        #for row in ws.iter_rows(min_row=2,max_col=10,max_row=2):
        for row in ws.iter_rows(min_row=2,max_col=10,max_row=ws.max_row):
            pub_title = row[0].value
            title_url = row[9].value
            
            # Insert a delay, for Wiley. They block after 120 actions in 60 seconds
            if (title_url.find("https://onlinelibrary.wiley.com") != -1):
                print ("Sleeping for Wiley URL...")
                time.sleep(3)
            
            # make sure it's not a blank row
            if title_url is not None:
                print ("Checking ",pub_title,"...")
                
                # Request the title_url
                try:
                    r = s.get(title_url, verify=False)
                except requests.ConnectionError:
                    status_code = "Error"
                    status_desc = "Connection Error"
                except requests.TooManyRedirects:
                    status_code = "Error"
                    status_desc = "Too Many Redirects"
                except requests.Timeout:
                    status_code = "Error"
                    status_desc = "Timeout"
                else:
                    status_code = r.status_code
                    status_desc = r.reason
                    
                print('Result:',status_code,' ',status_desc)
                
                results.append([pub_title,status_code,status_desc,title_url,in_file])
                # print (row[0].value, row[9].value)
            else:
                status_code = "Error"
                status_desc = "No URL in source file"
                results.append([pub_title,status_code,status_desc,title_url,in_file])

    # Write the results to .csv file
    with open (out_file, 'w', newline='') as f:
        wr = csv.writer(f, quoting=csv.QUOTE_ALL)
        wr.writerows(results)
