"""
get_kbart_website_status_list_threaded script

Written by: David Pettitt (Systems Administrator I, Geoffrey R Weller Library, University of Northern British Columbia)

Written for: Mandi Schwarz

Purpose: Parse a list of KBART .xlsx files, test the URLs, generate a new .csv file that lists the Journal Title, URL, Status, and name of file the URL came from

Distributed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License (http://creativecommons.org/licenses/by-nc-sa/3.0/). 

Questions? Contact Mandi at neufel2@gmail.com

"""

# must install 'requests' package, for HTTP requests
import requests
# must install 'openpyxl' package, for reading Excel files
from openpyxl import Workbook
from openpyxl import load_workbook
from pathlib import Path
import csv
import threading
from queue import Queue

def get_url_list(filelist):
    # empty list, will put journal titles & urls in this
    url_list = []
    
    for file in filelist:
        in_file = str(file)

        # Load workbook from file into 'wb' variable
        wb = load_workbook(in_file)

        # load primary worksheet from workbook into 'ws' variable
        ws = wb._sheets[0]
        
        for row in ws.iter_rows(min_row=2,max_col=10,max_row=ws.max_row):
            # row[0] is the pub_title, row[9] is the title_url
            url_list.append([row[0].value, row[9].value, str(file)])
    
    return url_list

def get_url_status(current_url):
    with requests.Session() as s:
        pub_title = current_url[0]
        title_url = current_url[1]
        in_file = current_url[2]
        # make sure it's not a blank row
        if title_url is not None:
            print ("Checking ",pub_title,"...")
            
            # Request the title_url
            try:
                r = s.head(title_url, verify=False)
            except requests.ConnectionError:
                status_code = "Error"
                status_desc = "Connection Error"
            except requests.TooManyRedirects:
                status_code = "Error"
                status_desc = "Too Many Redirects"
            except requests.Timeout:
                status_code = "Error"
                status_desc = "Timeout"
            else:
                status_code = r.status_code
                status_desc = r.reason
            
            print ("Checked ",pub_title)    
            print('Result:',status_code,' ',status_desc)
            
            # if status is a redirect, get the new url
            redir_code = set([301,302,303,307,308])
            if status_code in redir_code:
                redir_url = r.headers['Location']
                results.append([pub_title,status_code,status_desc,title_url,in_file,redir_url])
            else:
                results.append([pub_title,status_code,status_desc,title_url,in_file])

        else:
            status_code = "Error"
            status_desc = "No URL in source file"
            results.append([pub_title,status_code,status_desc,title_url,in_file])

# a function that is automatically called by an active thread?
def process_queue():
    while True:
        # pulls an item from the queue
        current_url = url_queue.get()
        # passes item to main function
        get_url_status(current_url)
        # indicates that queue item is complete
        url_queue.task_done()
        
# Suppress all the 'InsecureRequestWarning' from using verify=False in the requests
requests.urllib3.disable_warnings()

# This list will store all the results, to be written to .csv
results=[]
results.append(['Publication Title','Status Code','Status Description','URL','File','Redirect URL'])

# Output file
out_file = "get_kbart_website_status_list.csv"

# Get a list of .xlsx files in sub-directory
p = Path('.')
filelist = list(p.glob('*.xlsx'))

# creates new queue
url_queue = Queue()

# generate list of URLs to check, from files in s
url_list = get_url_list(filelist)

# Maximum number of active threads
max_threads = 50

for i in range(max_threads):
    t = threading.Thread(target=process_queue)
    t.daemon = True
    t.start()

# adds items from url_list list to queue
for url in url_list:
    url_queue.put(url)
    
# Forces script to wait for all threads to complete
url_queue.join()

# Write the results to .csv file
with open (out_file, 'w', newline='') as f:
    wr = csv.writer(f, quoting=csv.QUOTE_ALL)
    wr.writerows(results)